/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/IFRule.h"       /* <= own header */
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "led.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool on_off,hold;
uint16_t Medida;
/*==================[internal functions declaration]=========================*/
void selector(void){
	 Medida=HcSr04ReadDistanceCentimeters();
//	 UartSendBuffer(SERIAL_PORT_PC,UartItoa(Medida,10),2);
//	 UartSendString(SERIAL_PORT_PC,"cm \r\n");
}


void onoff(){

	 if(on_off==true){
			 on_off=false;

			 }
		 else{
			 on_off=true;
			 ITSE0803DisplayValue(0);

		 }
}

void hold1(){
	if(hold==false){
		hold=true;
	}
	 else{
		hold=false;
	 }
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;
	gpio_t ech,trig;
	ech=T_FIL2;
	trig=T_FIL3;
	on_off=true;
	hold=false;
	SystemClockInit();
	LedsInit();
	timer_config interrupcion;
	interrupcion.timer=TIMER_A;
	interrupcion.period=300;
	interrupcion.pFunc=selector;
	serial_config salida_serie;
	salida_serie.port=SERIAL_PORT_PC;
	salida_serie.baud_rate=9600;
	salida_serie.pSerial=NULL;
	HcSr04Init(ech, trig);
	ITSE0803Init(pins);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, onoff);
	SwitchActivInt(SWITCH_2, hold1);
	TimerInit(&interrupcion);
//	UartInit(&salida_serie);
	TimerStart(TIMER_A);


	 while(1){
		 if(on_off==1){
			 if(hold==false){
			 		ITSE0803DisplayValue(Medida);
		 		}
		 			else{}
		 	}
		 }


	 return 0;
}





/*==================[end of file]============================================*/

