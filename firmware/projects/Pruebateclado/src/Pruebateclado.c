/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Pruebateclado.h"       /* <= own header */
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"
#include "led.h"
#include "teclado.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool on_off,hold;
uint16_t tecla;
/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{

	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;
	gpio_t ech,trig;
	on_off=true;
	hold=false;
	SystemClockInit();
	initGPIO_keyboard_EDUCIAA();
	ITSE0803Init(pins);

	 while(1){
		 tecla=scanKeyboarECB();
		 switch(tecla)
		     	{
		     		case 2:
		     			ITSE0803DisplayValue(1);
		     		break;
		     		case 4:
		     			ITSE0803DisplayValue(2);
		     		break;
		     		case 8:
		     			ITSE0803DisplayValue(3);
		     		break;
		     		case 32:
		     			ITSE0803DisplayValue(4);
		     		break;
		     		case 64:
		     			ITSE0803DisplayValue(5);
		     		break;
		     		case 128:
		     			ITSE0803DisplayValue(6);
		     		break;
		     		case 512:
		     			ITSE0803DisplayValue(7);
		     		break;
		     		case 1024:
		     			ITSE0803DisplayValue(8);
		     		break;
		     		case 2048:
		     			ITSE0803DisplayValue(9);
		     		break;
		     		case 8192:
		     			ITSE0803DisplayValue(10);
		     		break;
		     		case 16384:
		     			ITSE0803DisplayValue(11);
		     		break;
		     		case 32768:
		     			ITSE0803DisplayValue(12);
		     		break;
		     	}



	 }
	 return 0;
}





/*==================[end of file]============================================*/

