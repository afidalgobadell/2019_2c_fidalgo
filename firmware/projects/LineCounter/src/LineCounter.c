/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "LineCounter.h"       /* <= own header */
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "Tcrt5000.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool on_off,hold; //variables para manejar las interrupciones
uint16_t LineCount;//Acumulador de conteo de lineas
bool PreviousState, ActualState;//Guardar estados del contador
gpio_t x;//pin para el contador
/*==================[internal functions declaration]=========================*/
//Funcion llamada por el timer
void selector(void){
	tecla= SwitchesRead();
		switch(tecla){
				case SWITCH_1 : {
							if(on_off==false){
										on_off=true;
										LineCount=0;
											}
							else{
								on_off=false;
								}
							if(on_off==false){
										ITSE0803Blank();
											}
				break;
								}
				case SWITCH_2 : {
							if(hold==false){
										hold=true;
											}
							else{
								hold=false;
								}
				break;
								}
				case SWITCH_3 : {
							LineCount=0;
				break;
								}
						}
					}




//Funciones llamadas por las interrupciones.
void onoff(){

	 if(on_off==true){
			 on_off=false;
			 LineCount=0;
			 ITSE0803Blank();
			 	 	 }
	 else{
			 on_off=true;
			 ITSE0803DisplayValue(0);
		 }
			}

void hold1(){

	if(hold==false){
			hold=true;
					}
	 else{
		 	 hold=false;
	 	 }
			}

void reset(){
	LineCount=0;
	ITSE0803DisplayValue(LineCount);
			}
/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
//Valor inicial de las variables
	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;
	x=T_FIL2;
	LineCount=0;
	on_off=true;
	hold=false;
	timer_config interrupcion;
	interrupcion.timer= TIMER_A;//Timer selecionado
	interrupcion.period=1;//Periodo en el que se repite el timer
	interrupcion.pFunc=selector();//Funcion a la que llama la interrupcion
//Inicializacion de drivers
	SystemClockInit();
	Tcrt5000Init(x);
	ITSE0803Init(pins);
	SwitchesInit();
	SwitchActivInt(SWITCH_1, onoff);
	SwitchActivInt(SWITCH_2, hold1);
	SwitchActivInt(SWITCH_3, reset);
	TimerInit(interrupcion);

	PreviousState=ActualState=Tcrt5000State();

	 while(1){
		 if(on_off==1){
		 ActualState=Tcrt5000State();
		 if(ActualState!=PreviousState){
		 		if(ActualState==0){
		 			if(hold==false)
		 				LineCount++;
		 			ITSE0803DisplayValue(LineCount);
		 		}
		 			else{}
		 	}
		 PreviousState=ActualState;
		 }}


	 return 0;



	 }

/*==================[end of file]============================================*/

