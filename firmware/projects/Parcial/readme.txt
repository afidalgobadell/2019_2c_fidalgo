Parcial

Aplicacion para convertir una señal analogica (presion) a valores digitales (muestreo de 250Hz),
 mostrar mediante un display un valor seleccionado (maximo, minimo, promedio) de un tiempo de 1 segundo de la señal
 tambien se entia el dato seleccionado por el puerto serie a la pc y prende el led correspondiente al dato que se seleccionó.
 El boton 1 es para seleccionar el maximo, 2 para el minimo y 3 para el promedio. La lectura se refresca cada segundo.
 Se debe conectar la señal analogica en el CH1 de entrada analogica.
 El display se debe conectar desde el LCD1 hasta el GPIO5 corresponsientes al bloque de pines de lado izquierdo.
