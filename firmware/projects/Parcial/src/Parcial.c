/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Digitalizar señal de un sensor de presión conectado al canal CH1 de la EDU-CIAA  tomando muestras a 250Hz por interrupciones.
 * El sensor de presión se comporta de manera lineal, con una salida de 0V para 0 mmHg y 3.3V para 200 mmHg.
Obtener el valor máximo, el valor mínimo y el promedio del último segundo de señal.
Seleccionar mediante tres botones de la EDU-CIAA, el parámetro a presentar en un display LCD (máximo, mínimo o promedio). El mismo valor se debe enviar a través de la UART a la PC (en ascii).
El sistema deberá informar mediante la activación de los led el valor seleccionado a presentar en el LCD (rojo: máximo, amarillo: mínimo, verde: promedio).
Si la presión máxima supera los 150 mmHg se deberá encender el red rojo del led RGB de la EDU-CIAA. Entre 150 y 50 mmHg el led azul y por debajo de 50 mmHg el led verde
 */

/*==================[inclusions]=============================================*/
#include "../inc/Parcial.h"       /* <= own header */
#include "analog_io.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "math.h"
#include "led.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t x,i,valor,segundo[250],promedio1s,maximo1s,minimo1s,ver;
float FACTORCONV=0.1953;//para convertir el valor digital a mmHg
/*==================[internal functions declaration]=========================*/
//Funcion usada para registrar los valores de la presión y convertirlos a la unidad correspondiente, tambien llena un vector con las muestras de un segundo.
 void muestreo(){
	 AnalogInputRead(CH1, valor);
	 valor=valor*FACTORCONV;
	 if(x<250){
		 segundo[x]=valor;
		 x++;
	 	 	 }
	 if(x==250){
		 x=0;
		 segundo[x]=valor;
	 	 	 	}
 	 	 	 	 }
 //Halla el maximo de los valores entre las muestras de un segundo
 uint16_t max(){
	 uint16_t maximo=segundo[0];
	 for(i=1;i<250;i++){
		 if(maximo<segundo[i]){
			 	 maximo=segundo[i];
		 	 	 	 	 	 }
	 	 	 	 	 	}
	 return maximo;
  	 	 	  }
 //Halla el minimo de los valores entre las muestras de un segundo
 uint16_t min(){
	 uint16_t minimo=segundo[0];
	 for(i=1;i<250;i++){
		 if(minimo>segundo[i]){
			 	 minimo=segundo[i];
		 	 	 	 	 	 }
	 	 	 	 	 	}
	 return minimo;
  	 	 	  }
 //Calcula el promedio de los valores tomados en un segundo
 uint16_t prom(){
	 uint16_t promedio=0;
	 for(i=0;i<250;i++){
		 promedio+=segundo[i];
	 	 	 	 	 	 }
	 promedio=promedio/250;
	 return promedio;
 	 	 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 1 cambia el valor de la variable ver la cual define que se muestra y envia (maximo)
 void muestromax(){
	 ver=1;
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 2 cambia el valor de la variable ver la cual define que se muestra y envia (minimo)
 void muestromin(){
	 ver=2;
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 3 cambia el valor de la variable ver la cual define que se muestra y envia (promedio)
 void muestroprom(){
	 ver=3;
 	 	 	 	 }

//Funcion llamada por el timer tomadeparametros la cual guarda el maximo minimo y promedio de un segundo, segun la seleccion de ver muestra (maximo, minimo o promedio)
 //envia el dato por el puerto serie y prende el led correspondiente a lo que se muestra. Tambien prende el led RGB con distintos colores segun el valor del maximo.
 void parametros(){
	 promedio1s=prom();
	 maximo1s=max();
	 minimo1s=min();
	 LedsOffAll();//Apago todos los leds prendidos en la llamada anterior
	 switch(ver)
	 		 		 	{
	 		 		     					     		case 1:
		 		 		     					     	ITSE0803DisplayValue(maximo1s);//muestro en display el maximo
		 		 		     					     	UartSendBuffer(SERIAL_PORT_PC,UartItoa(maximo1s,10),3);//Envio por el puerto serie el maximo
		 		 		     					     	LedOn(LED_1);//Prendo led rojo
	 		 		     					     		break;
	 		 		     					     		case 2:
			 		 		     					     ITSE0803DisplayValue(minimo1s);
			 		 		     					     UartSendBuffer(SERIAL_PORT_PC,UartItoa(minimo1s,10),3);//Envio por el puerto serie el minimo
			 		 		     					     LedOn(LED_2);//Prendo led amarillo
	 		 		     					     		break;
	 		 		     					     		case 3:
			 		 		     					     ITSE0803DisplayValue(promedio1s);
			 		 		     					     UartSendBuffer(SERIAL_PORT_PC,UartItoa(promedio1s,10),3);//Envio por el puerto serie el promedio
			 		 		     					     LedOn(LED_3);//prendo led verde
	 		 		     					     		break;
	 		 		 	}
	 if(maximo1s>150){
		 LedOn(LED_RGB_R);
	 }
	 if(maximo1s<50){
	 		 LedOn(LED_RGB_G);
	 	 }
	 if(maximo1s>50 && maximo1s<150){
	 		 LedOn(LED_RGB_B);
	 	 }
 }


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;
	//Structs para la definicion e inicializacion de timers, entrada analogica,y salida en serie
	analog_input_config entrada;
	entrada.input=CH1;//Canal de entrada seleccionado de la uart
	entrada.mode=AINPUTS_SINGLE_READ;
	entrada.pAnalogInput=NULL;
	AnalogInputInit(&entrada);
	timer_config muestreosenal;
	muestreosenal.timer=TIMER_A;//Systick
	muestreosenal.period=4;//Periodo de muestreo correspondiente a 250Hz
	muestreosenal.pFunc=muestreo;
	timer_config tomadeparametros;
	tomadeparametros.timer=TIMER_B;//RIT
	tomadeparametros.period=1000;//Periodo de toma y muestra de datos (1 segundo)
	tomadeparametros.pFunc=parametros;
	serial_config salida_serie;
	salida_serie.port=SERIAL_PORT_PC;
	salida_serie.baud_rate=9600;//velocidad de transmision en baudios
	salida_serie.pSerial=NULL;
	//Inicializacion de drivers
	SystemClockInit();
	ITSE0803Init(pins);
	UartInit(&salida_serie);
	SwitchesInit();
	SwitchActivInt(SWITCH_1,muestromax);
	SwitchActivInt(SWITCH_2,muestromin);
	SwitchActivInt(SWITCH_3,muestroprom);
	TimerInit(&muestreosenal);
	TimerInit(&tomadeparametros);
	//Inicio el conteo de los timers
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);

	 while(1){

	 	 	 }
	 return 0;
	 }

/*==================[end of file]============================================*/

