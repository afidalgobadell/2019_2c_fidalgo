/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*Digitalizar señal de un acelerómetro analógico conectado al canal CH1 de la EDU-CIAA  tomando muestras a 100Hz utilizando interrupciones.
Se desea implementar un filtrado digital de los datos, el cual responde a la siguiente expresión:
valor_filtrado[n] =  valor_filtrado[n-1] - (lpf_beta * (valor_filtrado[n-1] - valor_crudo[n]))
El parámetro lpf_beta caracteriza la respuesta en frecuencia del filtro. Seleccionar mediante tres botones de la EDU-CIAA, el parámetro a utilizar, siendo:
lpf_beta  = 0.25 si se presiona la tecla 2
lpf_beta  = 0.55 si se presiona la tecla 3
lpf_beta  = 0.75 si se presiona la tecla 4
El sistema deberá informar en el LCD el valor de lpf_beta actual (mostrar sólo la parte decimal)
El sistema debe mandar por puerto serie el valor crudo y el valor filtrado en cada muestra separados por una coma y un salto de línea final. Ej:
152,150

 */

/*==================[inclusions]=============================================*/
#include "../inc/Recuperatorio.h"       /* <= own header */
#include "analog_io.h"
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "gpio.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t opcion,valor_crudo,valor_filtrado[2];
float lpf_beta;//factor de filtrado
/*==================[internal functions declaration]=========================*/
//Funcion usada para registrar el valor del acelerometro, filtrar este valor y enviar por el puerto serie lo0s dos valores, el crudo y filtrado
 void muestreo_procesamiento_envio(){
	 AnalogInputRead(CH1, valor_crudo);
	 valor_filtrado[1]=  valor_filtrado[0] - (lpf_beta * (valor_filtrado[0] - valor_crudo));//filtrado que actualiza el valor del filtrado actual
	 UartSendBuffer(SERIAL_PORT_PC,UartItoa(valor_crudo,10),2);
	 UartSendString(SERIAL_PORT_PC,",");
	 UartSendBuffer(SERIAL_PORT_PC,UartItoa(valor_filtrado[1],10),2);
	 UartSendString(SERIAL_PORT_PC,"\r\n");//salto de renglon
	 valor_filtrado[0]=valor_filtrado[1];//pongo el valor filtrado actual en el valor filtrado anterior
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 2 cambia el valor de la variable lpf_beta y muestra la muestra por display
 void lpf25(){
	 lpf_beta=0.25;
	 ITSE0803DisplayValue(25);
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 3 cambia el valor de la variable lpf_beta y muestra la muestra por display
 void lpf55(){
	 lpf_beta=0.55;
	 ITSE0803DisplayValue(55);
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 4 cambia el valor de la variable lpf_beta y muestra la muestra por display
 void lpf75(){
	 lpf_beta=0.75;
	 ITSE0803DisplayValue(75);
 	 	 	 	 }

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;
	lpf_beta=0.25;//Propongo como valor de default
	//Structs para la definicion e inicializacion de timer, entrada analogica,y salida en serie
	analog_input_config entrada;
	entrada.input=CH1;//Canal de entrada seleccionado de la uart
	entrada.mode=AINPUTS_SINGLE_READ;
	entrada.pAnalogInput=NULL;
	timer_config senal;
	senal.timer=TIMER_A;//Systick
	senal.period=10;//Periodo de muestreo correspondiente a 100Hz
	senal.pFunc=muestreo_procesamiento_envio;
	serial_config salida_serie;
	salida_serie.port=SERIAL_PORT_PC;
	salida_serie.baud_rate=9600;//velocidad de transmision en baudios
	salida_serie.pSerial=NULL;
	//Inicializacion de drivers
	SystemClockInit();
	ITSE0803Init(pins);
	UartInit(&salida_serie);
	AnalogInputInit(&entrada);
	SwitchesInit();
	SwitchActivInt(SWITCH_2,lpf25);
	SwitchActivInt(SWITCH_3,lpf55);
	SwitchActivInt(SWITCH_4,lpf75);
	TimerInit(&senal);
	//Inicio el conteo del timer
	TimerStart(TIMER_A);

	 while(1){

	 	 	 }
	 return 0;
	 }

/*==================[end of file]============================================*/

