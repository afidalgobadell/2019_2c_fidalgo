/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Alejandro Fidalgo Badell
 *
 *
 *
 * Revisión:
 * 07-12-21: Versión inicial
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/* Diseñar un goniómetro digital de rodilla utilizando un potenciómetro solidario al eje de dos piezas móviles.
 *  Para esto se debe digitalizar la señal del punto medio del potenciómetro y calcular el ángulo,
 *   sabiendo a 0º la tensión de salida es 0V y a 180º es 3.3V.
 *  El sistema deberá además enviar a través de la UART tanto el valor de ángulo actual como el valor máximo medido hasta el momento
 *  (“XXº - YYº\r\n” donde XX es el valor de ángulo actual y YY el valor máximo de ángulo medido hasta ese instante)
 */

/*==================[inclusions]=============================================*/
#include "../inc/Examen_final_goniometro.h"       /* <= own header */

#include "analog_io.h"
#include "systemclock.h"
#include "gpio.h"
#include "switch.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t opcion,valor;
uint16_t valor_maximo=0;
uint8_t on_off=1; //variable para controlar por botones 1 y 2 si se realiza o no la medicion y envio de valores
float FACTORCONV=0.17578;//para convertir el valor digital a grados (180/1024)
/*==================[internal functions declaration]=========================*/
//Funcion usada para registrar el valor del goniometro y hallar el maximo valor hasta el momento
 void muestreo_procesamiento(){
	 if(on_off==1){
	 AnalogInputRead(CH1, valor);
	 valor=valor*FACTORCONV;
	 maximo();
	 	 	 	 	 }
	 else{}
 	 	 	 	 	 	 	 	 	 }
 //Funcion usada para enviar el valor actual del goniometro y el maximo valor hasta el momento con susrespectivas unidades y salto de linea
 void muestra_valores (){
	 if(on_off==1){
		 	 UartSendBuffer(SERIAL_PORT_PC,UartItoa(valor,10),2);
		 	 UartSendString(SERIAL_PORT_PC,"°-");
		 	 UartSendBuffer(SERIAL_PORT_PC,UartItoa(valor_maximo,10),2);
		 	 UartSendString(SERIAL_PORT_PC,"°\r\n");
	 	 	 	 	 }
	 else{}
 	 	 	 	 	 	 	 	 	 }
 //Halla el maximo entre los valores obtenidos hasta el momento
 void maximo(){
		 if(valor_maximo<valor)
			 	 valor_maximo=valor;
	 	 	 	 }

 //Funcion llamada por la interrupcion del boton 1 cambia el valor de la variable lpf_beta y muestra la muestra por display
 void on(){
	 on_off=1;
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 2 cambia el valor de la variable lpf_beta y muestra la muestra por display
 void off(){
	 on_off=0;
 	 	 	 	 }
 //Funcion llamada por la interrupcion del boton 3 cambia el valor de la variable lpf_beta y muestra la muestra por display
 void reset(){
	 valor=0;
	 valor_maximo=0;
 	 	 	 	 }

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Structs para la definicion e inicializacion de timer, entrada analogica,y salida en serie
	analog_input_config entrada;
	entrada.input=CH1;// Entrada analogica seleccionada
	entrada.mode=AINPUTS_SINGLE_READ; // lectura simple de valor analogico
	entrada.pAnalogInput=NULL;
	serial_config salida_serie;
	salida_serie.port=SERIAL_PORT_PC;
	salida_serie.baud_rate=9600;//velocidad de transmision en baudios
	salida_serie.pSerial=NULL;
	timer_config senal;
	senal.timer=TIMER_A;//Systick
	senal.period=10;//Periodo de muestreo correspondiente a 100Hz
	senal.pFunc=muestreo_procesamiento;// Funcion llamada por la interrupcion del timer A para la toma y procesamiento de los datos
	timer_config envio_valores;
	envio_valores.timer=TIMER_B;//RIT
	envio_valores.period=1000;//Periodo de envio de datos (1 segundo)
	envio_valores.pFunc=muestra_valores;// Funcion llamada por la interrupcion del timer B para el envio de los datos

	//Inicializacion de drivers
	SystemClockInit();
	UartInit(&salida_serie);
	AnalogInputInit(&entrada);
	SwitchesInit();
	SwitchActivInt(SWITCH_1,on);
	SwitchActivInt(SWITCH_2,off);
	SwitchActivInt(SWITCH_3,reset);
	TimerInit(&senal);
	TimerInit(&envio_valores);
	//Inicio el conteo del timer
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	 while(1){

	 	 	 }
	 return 0;
	 }

/*==================[end of file]============================================*/

