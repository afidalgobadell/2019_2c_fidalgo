Examen Final Goniometro

Aplicacion que lee el valor de un goniometro (hecho con un potenciometro que tiene como salida 0V para 0° y 3.3V para 180°),
este valor se ingresa por entrada analogica a la CIAA, se digitaliza a una frecuencia de 100Hz y se guarda el valor maximo
 encontrado hasta el momento, los valores actual y maximo se envian por puerto serie a una frecuencia de 1Hz en formato
 “XXº - YYº\r\n” siendo XX el valor actual y YY el maximo hasta el momento. Las teclas 1, 2 y 3 se utilizan para empezar la medicion,
 apagar la toma y muestra de valores y para resetear los valores hasta el momento respectivamente. 
