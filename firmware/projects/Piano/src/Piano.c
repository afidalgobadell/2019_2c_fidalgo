/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Piano.h"       /* <= own header */
#include "systemclock.h"
#include "DisplayITS_E0803.h"
#include "Tcrt5000.h"
#include "gpio.h"
#include "delay.h"
#include "switch.h"
#include "bool.h"
#include "timer.h"
#include "math.h"
#include "analog_io.h"
#include "teclado.h"
#include "uart.h"
/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
bool d1,r1,m1;
uint16_t x,y,i,nota;
uint32_t retardo;
float PI=3.14159;
uint16_t tecla,octava,senovalue[360];
//gpio_t ti;
/*==================[internal functions declaration]=========================*/
 void seno(){

	 	 	 if(x<360){
//	 	 		 GPIOOn(ti);
//	 	 		y=(400*sin(x*(2*PI)/360))+400;
//	 		  	GPIOOff(ti);
	 	 		AnalogOutputWrite(senovalue[x]);
//	 	 		ITSE0803DisplayValue(y);
	 	 		x++;
	 	 	 }
	 	 	 if(x==360){
//	 	 		 y=(400*sin(x*(2*PI)/360))+400;
	 	 		 x=0;
//	 	 		ITSE0803DisplayValue(y);
//	 	 		x++;
	 	 		 }


 }
 void llenarseno(){
	 for(i=0;i<361;i++){
		 y=(400*sin(i*(2*PI)/360))+400;
		 senovalue[i]=y;
	 	 	 	 	 	 }
 	 	 	 }
 void Octava2(){
 	octava=2;
 	 	 	 }

 void Octava1(){
 	octava=1;
 	 	 	 }
void notas(){
//	ITSE0803DisplayValue(nota);
								if(tecla!=0){
											switch(nota)
			 		     					     	{
			 		     					     		case 21:
	//		 		     					     			DO
			 		     					     		UartSendString(SERIAL_PORT_P2_CONNECTOR,"DO \r\n");
			 		     					     		ITSE0803DisplayValue(nota);
			 		     					     		break;
			 		     					     		case 22:
	//		 		     					     			RE
			 		     					   			UartSendString(SERIAL_PORT_P2_CONNECTOR,"RE \r\n");
			 		     					   			ITSE0803DisplayValue(nota);
			 		     					     		break;
			 		     					     		case 23:
	//		 		     					     			MI
			 		     					     		UartSendString(SERIAL_PORT_P2_CONNECTOR,"MI \r\n");
			 		     					     		ITSE0803DisplayValue(nota);
			 		     					     		break;
			 		     					     		case 24:
	//		 		     					     			FA
			 		     				     			UartSendString(SERIAL_PORT_P2_CONNECTOR,"FA \r\n");
			 		     				     			ITSE0803DisplayValue(nota);
			 		     					     		break;
			 		     					     		case 25:
	//		 		     					     			SOL
			 		     				     			UartSendString(SERIAL_PORT_P2_CONNECTOR,"SOL \r\n");
			 		     					     		break;
			 		     					     		case 26:
	//		 		     					     			LA
			 		     					   			UartSendString(SERIAL_PORT_P2_CONNECTOR,"LA \r\n");
			 		     					     		break;
			 		     					     		case 27:
	//		 		     					     			SI
			 		     					     		UartSendString(SERIAL_PORT_P2_CONNECTOR,"SI \r\n");
			 		     					     		break;
			 		     					     		case 28:
	//		 		     					     			DO
			 		     					     		UartSendString(SERIAL_PORT_P2_CONNECTOR,"DO \r\n");
			 		     					     		break;
			 		     					     	}
											nota=0;
										}

			}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	gpio_t pins[7];
	pins[0]=LCD1;
	pins[1]=LCD2;
	pins[2]=LCD3;
	pins[3]=LCD4;
	pins[4]=GPIO1;
	pins[5]=GPIO3;
	pins[6]=GPIO5;
	x=0;
	octava=1;
	llenarseno();
	SystemClockInit();
	ITSE0803Init(pins);
	timer_config interrupcion;
	interrupcion.timer=TIMER_A;
	interrupcion.period=1000;
	interrupcion.pFunc=notas;
	serial_config salida_serie;
	salida_serie.port=SERIAL_PORT_P2_CONNECTOR;
	salida_serie.baud_rate=115200;
	salida_serie.pSerial=NULL;
	AnalogOutputInit();
	SwitchesInit();
	initGPIO_keyboard_EDUCIAA();
	SwitchActivInt(SWITCH_1, Octava1);
	SwitchActivInt(SWITCH_2, Octava2);
	TimerInit(&interrupcion);
	UartInit(&salida_serie);
	TimerStart(TIMER_A);
	 while(1){
		 tecla=scanKeyboarECB();
		 switch(octava)  {
		 		     		case 1:
		 		     			switch(tecla)
		 		     					     	{
		 		     					     		case 2:
//		 		     					     			DO
		 		     					     	     retardo=76;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		 nota=11;
		 		     					     		break;
		 		     					     		case 4:
//		 		     					     			RE
			 		     					     	 retardo=67;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		 nota=12;
		 		     					     		break;
		 		     					     		case 8:
//		 		     					     			MI
			 		     					         retardo=60;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=13;
		 		     					     		break;
		 		     					     		case 32:
//		 		     					     			FA
			 		     					     	 retardo=56;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=14;
		 		     					     		break;
		 		     					     		case 64:
//		 		     					     			SOL
			 		     					     	 retardo=49;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=15;
		 		     					     		break;
		 		     					     		case 128:
//		 		     					     			LA
			 		     					     	 retardo=42;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=16;
		 		     					     		break;
		 		     					     		case 512:
//		 		     					     			SI
			 		     					     	 retardo=36;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=17;
		 		     					     		break;
		 		     					     		case 1024:
//		 		     					     			DO
			 		     					     	 retardo=34;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=18;
		 		     					     		break;
		 		     					     	}

		 		     		break;


		 		     		case 2:
		 		     			switch(tecla)
		 		     					     	{
		 		     					     		case 2:
//		 		     					     			DO
		 		     					     	     retardo=34;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=21;
		 		     					     		break;
		 		     					     		case 4:
//		 		     					     			RE
			 		     					     	 retardo=30;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=22;
		 		     					     		break;
		 		     					     		case 8:
//		 		     					     			MI
			 		     					         retardo=25;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=23;
		 		     					     		break;
		 		     					     		case 32:
//		 		     					     			FA
			 		     					     	 retardo=23;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=24;
		 		     					     		break;
		 		     					     		case 64:
//		 		     					     			SOL
			 		     					     	 retardo=20;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=25;
		 		     					     		break;
		 		     					     		case 128:
//		 		     					     			LA
			 		     					     	 retardo=17;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=26;
		 		     					     		break;
		 		     					     		case 512:
//		 		     					     			SI
			 		     					     	 retardo=14;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=27;
		 		     					     		break;
		 		     					     		case 1024:
//		 		     					     			DO
			 		     					     	 retardo=13;
		 		     					     		 DelayUs(retardo);
		 		     					     		 seno();
		 		     					     		nota=28;
		 		     					     		break;
		 		     					     	}
		 		     		break;
		 }
	 }
	 return 0;
	 }

/*==================[end of file]============================================*/

