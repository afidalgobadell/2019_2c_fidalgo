Examen Final Medidor de Volumen

Aplicacion que lee la altura de liquido en un vaso y calcula el volumen de liquido en el mismo,
 el valor de volumen se actualiza y envia por puerto serie a una frecuencia de 10Hz.
 El valor enviado por puerto serie incluye unidad de volumen (cm3) y un salto de linea por cada valor.
