/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * Alejandro Fidalgo
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
/*SISTEMA DE MEDICION DE VOLUMEN*/
/* Se tiene un vaso de 4cm de diámetro y 12 cm de alto, se coloca el sensor de distancia en la parte superior
 * Calcule a partir de la distancia medida a la superficie del agua la cantidad de la misma que contiene el vaso.
 *  Envíe este dato 10 veces por segundo por la UART a la PC en el siguiente formato: xx + “ cm3” (uno por renglón).
 */
/*==================[inclusions]=============================================*/
#include "../inc/Examen_final_volumen.h"       /* <= own header */
#include "hc_sr4.h"
#include "systemclock.h"
#include "gpio.h"
#include "delay.h"
#include "bool.h"
#include "timer.h"
#include "uart.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/
uint16_t diametro=4; //diametro del vaso en cm
uint16_t alto=12; //alto del vaso en cm
int16_t medida;
float volumen;
// Puertos en los que se conecta el sensor hc_sr4
gpio_t ech=GPIO1;
gpio_t trig=GPIO3;

/*==================[internal functions declaration]=========================*/
// Funcion encargada de leer la distancia, calcular el volumen de liquido y enviarlo por la uart
void toma_datos_y_salida_serie(void){
	 medida=HcSr04ReadDistanceCentimeters(); //toma de distancia en centimetros
	 calculo_volumen(diametro,alto,medida); //calculo del volumen de liquido
	 UartSendBuffer(SERIAL_PORT_PC,UartItoa(volumen,10),4); // envio de volumen por puerto serie
	 UartSendString(SERIAL_PORT_PC,"cm3 \r\n");
}
// Funcion que calcula el volumen de liquido presente en el vaso
void calculo_volumen (uint16_t diam, uint16_t alt, int16_t med){
	volumen=(3.14*(diam/2)*(diam/2))*(alt-med); //PI*r2*z
}


/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	//Structs para la definicion e inicializacion de timers, detector IF y salida en serie
	timer_config interrupcion;
	interrupcion.timer=TIMER_A; //Systick
	interrupcion.period=100; //periodo en milisegundos para una frecuencia de 10Hz
	interrupcion.pFunc=toma_datos_y_salida_serie;

	serial_config salida_serie;
	salida_serie.port=SERIAL_PORT_PC;
	salida_serie.baud_rate=9600;//velocidad de transmision en baudios
	salida_serie.pSerial=NULL;
	//Inicializacion de drivers
	SystemClockInit();
	UartInit(&salida_serie);
	HcSr04Init(ech, trig);
	TimerInit(&interrupcion);
	//Inicio el conteo de los timers
	TimerStart(TIMER_A);

	 while(1){

	 	 	 }
	 return 0;
	 }

/*==================[end of file]============================================*/

