/*
 * Tcrt5000.h
 *
 *  Created on: 6 sep. 2019
 *      Author: Alumno
 */

#ifndef TCRT5000_H_
#define TCRT5000_H_
#include "gpio.h"

bool Tcrt5000Init(gpio_t dout);
bool Tcrt5000State(void);
bool Tcrt5000Deinit();



#endif /* TCRT5000_H_ */
