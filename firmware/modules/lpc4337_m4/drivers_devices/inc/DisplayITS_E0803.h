/*
 * DisplayITS_E0803.h
 *
 *  Created on: 6/09/2019
 *  Author: Alejandro Fidalgo Badell
 */

#ifndef DisplayITS_E0803_H
#define DisplayITS_E0803_H

/*==================[inclusions]=============================================*/
#include "gpio.h"
#include "bool.h"
#include <stdint.h>


/*==================[external functions declaration]=========================*/

/** @brief Initialization function of EDU-CIAA ITSE0803
 *
 * Set direction and initial state of lcd ports
 *
 * @param[in] pins
 *
 * @return TRUE if no error
 */
bool ITSE0803Init(gpio_t *pins);

/* @brief function to set value
 *
 * @param[in] valor
 *
 * @return TRUE if no error
 */

bool ITSE0803DisplayValue(uint16_t valor);

/** @brief function to read the actual value of the display
 *
 * @param[in] No parameter
 *
 * @return actual value of display
 */
uint16_t ITSE0803ReadValue(void);
/** @brief Deinitialization function of EDU-CIAA ITSE0803
 *
 * @param[in] pins to deinit
 *
 * @return TRUE if no error
 */
bool ITSE0803Deinit(gpio_t *pins);
/** @brief Conversor of decimal code to Bcd
 *
 * @param[in]  digit in decimal
 *
 * @return No parameter
 */
void DecimalToBcd(uint8_t digit);
/** @brief Blank the LCD display
 *
 * @param[in] No parameter
 *
 * @return No parameter
 */
void ITSE0803Blank(void);


#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_DISPLAYITS_E0803_H_ */
