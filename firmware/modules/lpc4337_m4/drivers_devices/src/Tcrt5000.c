/*
 * Tcrt5000.c
 *
 *  Created on: 6 sep. 2019
 *      Author: Alumno
 */

#include "Tcrt5000.h"
#include "bool.h"

gpio_t MyPin;

bool Tcrt5000Init(gpio_t dout){
	if(dout>LEDRGB_B){
		GPIOInit(dout, GPIO_INPUT);
		MyPin=dout;
		return (true);}

	else{
		return (false);}
}


bool Tcrt5000State(void){

	return GPIORead(MyPin);
}

bool Tcrt5000Deinit(){
	GPIODeinit();
}






