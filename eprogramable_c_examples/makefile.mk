########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
#PROYECTO_ACTIVO = 1_hola_mundo
#NOMBRE_EJECUTABLE = hola.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = 4_Guia1_ejercicio_7
#NOMBRE_EJECUTABLE = guia1_7.exe
#PROYECTO_ACTIVO = 4_Guia1_ejercicio_9
#NOMBRE_EJECUTABLE = guia1_9.exe
#PROYECTO_ACTIVO = 4_Guia1_ejercicio_12
#NOMBRE_EJECUTABLE = guia1_12.exe
#PROYECTO_ACTIVO = 4_Guia1_ejercicio_14
#NOMBRE_EJECUTABLE = guia1_14.exe
#PROYECTO_ACTIVO = 4_Guia1_ejercicio_16
#NOMBRE_EJECUTABLE = guia1_16.exe
PROYECTO_ACTIVO = 4_Guia1_ejercicio_integrador
NOMBRE_EJECUTABLE = guia1_integrador.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe