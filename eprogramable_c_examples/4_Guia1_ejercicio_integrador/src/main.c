/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../4_Guia1_ejercicio_7/inc/main.h"

#include "stdio.h"
#include "stdint.h"
#include "string.h"
/*==================[macros and definitions]=================================*/
typedef struct {
    uint8_t n_led;    //    indica el número de led a controlar
    uint8_t n_ciclos; // indica la cantidad de cilcos de encendido/apagado
    uint8_t periodo;  //  indica el tiempo de cada ciclo
    uint8_t mode;    //   ON=1 , OFF=0 , TOGGLE
}my_leds;
typedef struct
{
    char * txt ;      	/* Etiqueta de la opción */
    void (*doit)() ;             /* Función a ejecutar en caso de seleccionar esa opción */
} menuEntry;
typedef struct
{
	uint8_t port;				/*!< GPIO port number */
	uint8_t pin;				/*!< GPIO pin number */
	uint8_t dir;				/*!< GPIO direction ‘0’ IN;  ‘1’ OUT */
} gpioConf_t;

/*==================[internal functions declaration]=========================*/

void controlador (my_leds *confled){
	if (confled->mode==1){
		uint8_t num1=1;
		while(confled->n_led=!num1){
			num1++;}
			printf("Enciende led d% ",num1);}

	else{ if (confled->mode==0){
			uint8_t num2=1;
			while(confled->n_led=!num2){
						num2++;}
			printf("Apaga led d% ",num2);}
		  else{ uint8_t j;
		  	  j=0;
		  	  uint32_t retardo,i;
		  	  retardo=50000;
		  	  i=0;
		  	  do{ if(confled->n_led==1){
				  printf("Toggle led 1");}
				  else{ if(confled->n_led==2){
					  printf("Toggle led 2");}
				  else{
					  printf("Toggle led 3");}}
		  	  j++;
		  	  while(i<retardo){
		  		  	  	  i++;}
		  	  	 }
		  	  while(j<confled->n_ciclos);
			  }

			}
	return 0;
	}
void EjecutarMenu(menuEntry * menu , int option){
	switch(option){
	case 1:
		menu[0].doit();
	break;
	case 2:
		menu[1].doit();
	break;
	case 3:
		menu[3].doit();
		break;
}

}

//Escriba una función que reciba un dato de 32 bits,  la cantidad de dígitos de salida y un puntero a un arreglo donde
//se almacene los n dígitos. La función deberá convertir el dato recibido a BCD, guardando cada uno de los dígitos de
//salida en el arreglo pasado como puntero.
void BinaryToBcd (uint32_t data, uint8_t digits, uint8_t * bcd_number){
	 uint32_t aux;
	 aux= data;
	 uint8_t j;
	 for(j=0;j<digits;j++){
		 bcd_number[j]= aux%10;
		 aux=aux/10;
		 }



	 	 	 	 	 													}
 void ConvBcd( uint8_t bcd,gpioConf_t* pos){
	 uint8_t b0,b1,b2,b3,j;
	 b3=(bcd<<3);
	 b2=(bcd<<2);
	 b2&=0x01;
	 b1=(bcd<<1);
	 b1&=0x01;
	 b0=bcd;
	 b0&=0x01;
	 uint8_t b[]={b0,b1,b2,b3};

	 for(j=0;j<4;j++){
	 printf("B %d",j);
	 printf("%d",b[j]);
	 printf("-> ");
	 printf("puerto  %d",pos[j].port);
	 printf(".");
	 printf("%d",pos[j].pin);
	 printf("\n");   }
 }



int main(void)
{

// Integrador Parte B
	menuEntry menuPrincipal [] = {
	   	 { "etiqueta" , controlador},
	};



	return 0;
}

/*==================[end of file]============================================*/
